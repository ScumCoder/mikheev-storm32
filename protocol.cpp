#include "protocol.h"
#include "mbed_assert.h"
#include "cstring"

Protocol::Protocol()
{
    Purge();
}

void Protocol::Purge()
{
    m_state = State::waitingForFirstByte;
    ::memset(m_data, 0x00, sizeof(m_data));
    m_dataSize = 0;
}

bool Protocol::Feed(uint8_t byte)
{
    switch (m_state)
    {
    case State::waitingForFirstByte:
    {
        if(byte & 0x80)
        {
            MBED_ASSERT(m_dataSize == 0);
            m_state = State::gettingData;
            m_data[m_dataSize] = byte;
            m_dataSize ++;
        }
        else
        {
            //m_stats[Stat_bytesDropped_noHiBit] ++;
        }

        return false;
    }

    case State::gettingData:
    {
        if(byte & 0x80)
        {
            //m_stats[Stat_bytesDropped_hiBit] += m_dataSize + 1;
            Purge();
            return false;
        }

        MBED_ASSERT(1 <= m_dataSize);
        MBED_ASSERT(m_dataSize < packetSize);
        m_data[m_dataSize] = byte;
        m_dataSize ++;

        if(m_dataSize == packetSize)
        {
            uint8_t crc {0};
            for(int i = 0; i < m_dataSize - 1; i ++)
                crc += m_data[i];
            crc &= 0x7f;

            if(crc != byte)
            {
                //scWarning() << "CRC mismatch: actual " << crc << ", got " << byte;
                //m_stats[Stat_crcMismatch] ++;
                Purge();
                return false;
            }

            m_state = State::waitingForFirstByte;
            m_dataSize = 0;
            //m_stats[Stat_packetsOk] ++;
            return true;
        }

        return false;
    }
    }

    //SC_THROW(m_state);
    return false;
}

void Protocol::GetResult(Axis *axisPtr, uint32_t *posPtr) const
{
    if(posPtr)
    {
        uint32_t pos {0};
        for(int idx = 0; idx < packetSize - 2; idx ++)
        {
            uint32_t t {m_data[idx]};
            t &= 0x7f;
            t <<= idx * 7;
            pos |= t;
        }
        (*posPtr) = pos;
    }

    if(axisPtr)
    {
        uint8_t axis {m_data[packetSize - 2]};
        axis &= 0x7f;
        (*axisPtr) = static_cast<Axis>(axis);
    }
}
