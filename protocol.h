#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "stdint.h"

// Говнопротокол, основанный на протоколе ММД Радиоавионики 2011 г.
class Protocol
{
public:
    Protocol(void);

    // 21 бит - uint32_t с положением
    // 7 бит - uint8_t с номером оси
    // 7 бит - uint8_t с crc
    static const int packetSize {5};
    static const uint32_t maxPos {0x1fffffu};

    enum class Axis : uint8_t
    {
        Invalid,
        Yaw,
        Roll,
        Pitch
    };

private:
    enum class State
    {
        waitingForFirstByte,
        gettingData
    };

    State m_state {State::waitingForFirstByte};
    uint8_t m_data[packetSize];
    int m_dataSize {0};

public:
    void Purge(void);
    bool Feed(uint8_t byte);
    void GetResult(Axis *axis, uint32_t *pos) const;
};

#endif // PROTOCOL_H
