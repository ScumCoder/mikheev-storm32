#include "mbed.h"
#include "protocol.h"

namespace
{
// Светодиоды
DigitalOut greenLed(PB_12);
DigitalOut redLed(PB_13);
Timeout greenLedOffer;
Timeout redLedOffer;

// Оси
enum AxisIdx
{
    Axis_Yaw,
    Axis_Roll,
    Axis_Pitch,

    Axes_count
};

// Обмотки
enum CoilIdx
{
    Coil_a,
    Coil_b,
    Coil_c,

    Coils_count
};

AxisIdx Axis2AxisIdx(Protocol::Axis axis)
{
    switch (axis)
    {
    case Protocol::Axis::Invalid: break;
#define SC_SWITCH_CASE(_X) case Protocol::Axis::_X: return Axis_ ## _X
        SC_SWITCH_CASE(Yaw);
        SC_SWITCH_CASE(Roll);
        SC_SWITCH_CASE(Pitch);
#undef SC_SWITCH_CASE
    }
    return Axes_count;
}

// Последовательный порт в компьютер
BufferedSerial uart(PC_10, PC_11, 115200);

// Потенциометрические входы (для джойстиков)
//AnalogIn ain1(PC_0);
//AnalogIn ain2(PC_1);
//AnalogIn ain3(PC_2);

void GreenLedOff()
{
    greenLed = 0;
}

void RedLedOff()
{
    redLed = 0;
}

void SetCoil(PwmOut *pwm,
             float angle, // [0, 2*pi]
             float force) // [0, 1]
{
    angle = (sin(angle) + 1.0f) / 2.0f;
    pwm->write(force * angle);
}

void Rotate(PwmOut *const pwms[Coils_count],
            float angle, // [0, 1] соответствует углу вектора магнитного поля
            float force) // [0, 1] соответствует удерживающему току от 0 до максимального
{
    static const float pi {3.14159265358979323846f};
    angle *= 2.0f * pi;
    SetCoil(pwms[Coil_a], angle, force);
    angle += (2.0f * pi) / 3.0f;
    SetCoil(pwms[Coil_b], angle, force);
    angle += (2.0f * pi) / 3.0f;
    SetCoil(pwms[Coil_c], angle, force);
}
} // anon namespace

int main()
{
    static const float force {0.6}; // [0, 1] соответствует удерживающему току от 0 до максимального. На максимальном (при напряжении 12 В) обмотки перегреваются за 1-2 мин.
    static const float pwmPeriod {0.00005}; // в даташите DRV8313 написано "не более 250 кГц", в разделе "Typical Applications" в качестве примера указано 25 кГц

    redLed = 1;
    redLedOffer.attach(RedLedOff, 150ms);
    ThisThread::sleep_for(300ms);


    // ШИМ-выходы на обмотки
    // См. https://github.com/olliw42/storm32bgc/blob/master/storm32-bgc/storm32-bgc-v130-eagle-gerber-files-20140322/STorM32%20BGC%20v130.sch

    // Внешняя ось
    PwmOut yaw_a(PB_9);
    PwmOut yaw_b(PA_1);
    PwmOut yaw_c(PB_8);

    PwmOut roll_a(PA_6);
    PwmOut roll_b(PA_3);
    PwmOut roll_c(PA_2);

    // Внутр. ось
    PwmOut pitch_a(PB_1);
    PwmOut pitch_b(PB_0);
    PwmOut pitch_c(PA_7);

    PwmOut *const pwms[Coils_count][Axes_count]
    {
        {&yaw_a, &yaw_b, &yaw_c},
        {&roll_a, &roll_b, &roll_c},
        {&pitch_a, &pitch_b, &pitch_c}
    };


    greenLed = 1;
    greenLedOffer.attach(GreenLedOff, 150ms);
    ThisThread::sleep_for(300ms);


    for(int coil = 0; coil < Coils_count; coil ++)
        for(int axis = 0; axis < Axes_count; axis ++)
            pwms[coil][axis]->period(pwmPeriod);


    redLed = 1;
    redLedOffer.attach(RedLedOff, 150ms);
    ThisThread::sleep_for(300ms);


    Protocol protocol;
    while(1)
    {
        uint8_t byte {0};
        const ssize_t ret {uart.read(&byte, sizeof(byte))};
        if(ret != sizeof(byte))
        {
            // WTF?
            redLed = 1;
            redLedOffer.attach(RedLedOff, 800ms);
        }
        else
        {
            if(protocol.Feed(byte))
            {
                Protocol::Axis axis {Protocol::Axis::Invalid};
                uint32_t pos {0};
                protocol.GetResult(&axis, &pos);

                const AxisIdx axisIdx {Axis2AxisIdx(axis)};
                if(axisIdx >= 0 && axisIdx < Axes_count && pos <= Protocol::maxPos)
                {
                    greenLed = 1;
                    greenLedOffer.attach(GreenLedOff, 500ms);

                    float angle = pos;
                    angle /= Protocol::maxPos;
                    Rotate(pwms[axisIdx], angle, force);
                }
                else
                {
                    // Ошибка протокола
                    redLed = 1;
                    redLedOffer.attach(RedLedOff, 500ms);
                }
            }
        }
    }
}
